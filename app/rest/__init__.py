from flask import Blueprint

rest_bp = Blueprint('rest_bp', __name__, url_prefix='/api')
from . import views
