import datetime
from flask import request
from ..models import Calls, db
from . import rest_bp


@rest_bp.route('/calls/', methods=['POST'])
def add_call():
    call = Calls()
    call.caller_id = request.form['caller_id']
    call.called_id = request.form['called_id']
    call.calltime = datetime.datetime.now()
    db.session.add(call)
    db.session.commit()
    return "CallerID is {} and CalledID is {}".format(call.caller_id, call.called_id)


@rest_bp.route('/calls/', methods=['GET'])
def index():
    return "This is the REST url"
