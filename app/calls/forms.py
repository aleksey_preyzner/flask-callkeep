from flask_wtf import FlaskForm
from wtforms import validators
from wtforms.fields import StringField
from wtforms.widgets import TextArea


class CommentForm(FlaskForm):
    comment = StringField(u'Comment', validators=[validators.input_required(),
                          validators.Length(min=10, max=250)],
                          widget=TextArea())
