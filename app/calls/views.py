import datetime
from flask import render_template, request
from flask import current_app as app
from flask_login import login_required
from ..models import db, Calls, CallsComments
from . import calls_bp
from .forms import CommentForm



@calls_bp.route('/', methods=['GET'])
@calls_bp.route('/calls/', methods=['GET'])
@calls_bp.route('/calls/<int:page>', methods=['GET'])
def index(page=1):
    calls = Calls.query.filter_by(is_called_back = False).order_by("calltime DESC").paginate(page,app.config['ITEMS_PER_PAGE'])
    return render_template('calls/index.html', calls=calls)


@calls_bp.route('/calls/call/<int:call_id>', methods=['GET', 'POST'])
@login_required
def call_detail(call_id):
    call = Calls.query.get(call_id)
    if request.method == 'POST':
        if (request.form.get('comment')):
            add_comment(call.id)
        if (request.form.get('called_back')):
            set_called_back(call.id)

    call_comments = CallsComments.query.filter_by(call_id=call_id).order_by("comment_date DESC")
    form = CommentForm()
    return render_template('calls/detail.html', call=call, form=form,
                           call_comments=call_comments)


def add_comment(id):
    comment = CallsComments()
    comment.text = request.form['comment']
    comment.call_id = id
    comment.user_id = 1
    comment.comment_date = datetime.datetime.now()

    db.session.add(comment)
    db.session.commit()


def set_called_back(id):
    call = Calls.query.get(id)
    call.is_called_back = 1
    db.session.commit()
