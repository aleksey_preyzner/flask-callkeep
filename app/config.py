#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib

class Config:

    """Main config class
    """

    SECRET_KEY = "asoirfvnsyDtKa]pcLRDpaecQDweGr]piq2394we4urmasd"
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # pagination settings/ Items on per page/
    ITEMS_PER_PAGE = 30

    @staticmethod
    def init_app(app):
        pass


class ConfigDev(Config):

    """Config for development server
    """

    params = urllib.parse.quote_plus("DRIVER={ODBC Driver 17 for SQL Server};"
                                     "SERVER=APP\SQLEXPRESS;"
                                     "DATABASE=avanta_calls_dev;"
                                     "UID=pbx;"
                                     "PWD={Ckj;ysqGfhjkm};")
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mssql+pyodbc:///?odbc_connect={}'.format(params)
    SQLALCHEMY_ECHO = True


class ConfigProd(Config):

    """Config for production server
    """
    params = urllib.parse.quote_plus("DRIVER={ODBC Driver 17 for SQL Server};"
                                     "SERVER=APP\SQLEXPRESS;"
                                     "DATABASE=avanta_calls_work;"
                                     "UID=pbx;"
                                     "PWD={Ckj;ysqGfhjkm};")
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'mssql+pyodbc:///?odbc_connect={}'.format(params)
    SQLALCHEMY_ECHO = False
    WTF_CSRF_SECRET_KEY = 'xcHGMszwyfPRTdTRou8j0ik1QVVTwRQ85lAiPMNkFcOCAjwIRpy'

config = {
'development': ConfigDev,
'production': ConfigProd
}
