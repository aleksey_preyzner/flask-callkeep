import datetime
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash


db = SQLAlchemy()


class Calls(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    caller_id = db.Column(db.String(11))
    called_id = db.Column(db.String(11))
    calltime = db.Column(db.DateTime)
    is_called_back = db.Column(db.Boolean, nullable=False, default=False)
    # TODO добавить новое поле с id пользователя, который отзвонился.
    # callback_user = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    # TODO id в какой отдел звонили. В эндпоинте надо знать Department.id
    comment = db.relationship('CallsComments', backref='call', lazy=True)
    # TODO количество пропущенных звонков за сегодня.
    # day_call_count = db.Column(db.Integer)


class CallsComments(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    call_id = db.Column(db.Integer, db.ForeignKey('calls.id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    comment_date = db.Column(db.DateTime, default=datetime.datetime.now())
    text = db.Column(db.String(255), nullable=False)


class Users(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(80), nullable=False)
    last_name = db.Column(db.String(80), nullable=False)
    username = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(128), nullable=False, default="")
    email = db.Column(db.String(120), unique=True, nullable=False)
    # TODO добавить аватар/фото
    # image = db.Column(db.LargeBinary)
    # TODO флаг администратора
    # is_stuff = db.Column(db.Boolean, nullable=False, default=False)

    def __repr__(self):
        return "User {}".format(self.username)

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)


class Groups(db.Model):
    id = db.Column(db.Integer, primary_key=True)


class Departments(db.Model):
    id = db.Column(db.Integer, primary_key=True)
