from flask import render_template, request, redirect, url_for
from flask import current_app as app
from flask_login import logout_user, current_user, login_user,  login_required
from . import users_bp
from .forms import RegistrationForm, LoginForm
from ..models import db, Users

@users_bp.route("/auth/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('calls_bp.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = Users.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('calls_bp.index'))
    return render_template("auth/login.html", form=form)


@users_bp.route("/auth/register", methods=["GET", "POST"])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = Users(
            first_name=form.first_name.data,
            last_name=form.last_name.data,
            username=form.username.data,
            email=form.email.data
        )
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        login_user(user, remember=False)
        return redirect(url_for('calls_bp.index'))
    return render_template("auth/registration.html", form=form)


@users_bp.route("/auth/restore", methods=["GET", "POST"])
def restore():
    return render_template("auth/lostpassword.html")


@users_bp.route("/auth/logout", methods=["GET"])
@login_required
def logout():
    logout_user()
    return redirect(url_for('calls_bp.index'))
