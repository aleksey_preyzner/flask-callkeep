import os
from flask import Flask, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_wtf.csrf import CSRFProtect
from .models import db, Users
from .config import config


login_manager = LoginManager()
login_manager.login_view = "users_bp.login"
csrf = CSRFProtect()


@login_manager.user_loader
def load_user(id):
    return Users.query.get(int(id))

def create_app():
    app = Flask(__name__)
    migrate = Migrate(app, db)
    app.config.from_object(config[os.getenv('FLASK_ENV')])
    config[os.getenv('FLASK_ENV')].init_app(app)
    login_manager.init_app(app)
    db.init_app(app)
    csrf.init_app(app)

    # import blueprints
    from .admin import admin_bp
    app.register_blueprint(admin_bp)

    from .calls import calls_bp
    app.register_blueprint(calls_bp)

    from .users import users_bp
    app.register_blueprint(users_bp)

    from .rest import rest_bp
    app.register_blueprint(rest_bp)

    return app
